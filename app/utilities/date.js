/**
 * @param {Date} date
 * @returns {string}
 */
function formatMonth(date) {
  return new Intl.DateTimeFormat('en-GB', { month: 'long' }).format(date);
}

/**
 * @param {Date} date
 * @returns {string}
 */
function formatShortDate(date) {
  const weekday = new Intl.DateTimeFormat('en-GB', { weekday: 'long' }).format(date);

  const dayNumber = date.getDate();
  const suffix = { one: "st", two: "nd", few: "rd", other: "th" }[new Intl.PluralRules("en", {type: "ordinal"}).select(dayNumber)];

  return weekday + ' ' + dayNumber + suffix;
}

/**
 * @param {Date} date
 * @returns {string}
 */
function formatLongDate(date) {
  return formatShortDate(date) + ' ' + formatMonth(date);
}

/**
 * @param {string | null | undefined} time
 * @returns {string}
 */
function formatTime(time) {
  if (time === null || time === undefined)
    return '';

  const components = time.split(':', 2);

  let hour = parseInt(components[0], 10);
  const minute = components.length === 2 ? parseInt(components[1], 10) : 0;

  const date = new Date();
  date.setHours(hour, minute, 0, 0);

  const formatted =  new Intl.DateTimeFormat(
    'en',
    { hour12: true, hour: 'numeric', minute: '2-digit' }
  ).format(date);

  switch (formatted) {
    case '12:00 pm':
      return '12 noon';

    case '12:00 am':
      return '12 midnight';

    default:
      return formatted.replace(/ /, ' ').toLowerCase();
  }
}

/**
 * @param {string | null | undefined} time
 * @returns {string}
 */
function padTime(time) {
  if (time === null || time === undefined)
    return '';

  const components = time.split(':', 2);

  let hour = parseInt(components[0], 10);
  if (hour < 10)
    hour = `0${hour}`;

  let minute = components.length === 2 ? parseInt(components[1], 10) : 0;
  if (minute < 10)
    minute = `0${minute}`;

  return `${hour}:${minute}`;
}
