/**
 * @callback AlertButtonHandler
 * @param {*} value
 * @return {boolean | void | object}
 */

/**
 * @typedef AlertButton
 * @property {string} text
 * @property {string} [role]
 * @property {string | string[]} [cssClass]
 * @property {AlertButtonHandler} [handler]
 */

/**
 *
 * @param {(AlertButton | string)[]} buttons
 * @param {string} message
 * @param {string?} header
 * @param {string?} subheader
 * @return {Promise<void>}
 */
async function presentAlert(buttons, message, header, subheader) {
  const alert = document.createElement('ion-alert');
  alert.header = header;
  alert.subHeader = subheader;
  alert.message = message;
  alert.buttons = buttons;

  document.body.appendChild(alert);
  // noinspection JSUnresolvedFunction
  return alert.present();
}

/**
 * @param {string} message
 * @param {number} [duration=2000]
 * @return {Promise<void>}
 */
async function presentToast(message, duration) {
  if (!duration)
    duration = 2000;

  if (console && console.log)
    console.log(message);

  const toast = document.createElement('ion-toast');
  toast.message = message;
  toast.duration = duration;

  document.body.appendChild(toast);
  // noinspection JSUnresolvedFunction
  return toast.present();
}
