/**
 * @typedef {'forward' | 'back' | 'root'} Ionic~RouterDirection
 */

/**
 * @callback Ionic~RouterBack
 * @return Promise<void>
 */

/**
 * @callback Ionic~AnimationBuilder
 * @param {*} baseEl
 * @param {*?} opts
 * @return Animation
 */

/**
 * @typedef {object} Ionic~Toggle
 * @property {boolean} checked
 */

/**
 * @callback Ionic~RouterPush
 * @param {string} url
 * @param {RouterDirection?} direction
 * @param {AnimationBuilder?} animation
 * @return {Promise<boolean>}
 */

/**
 * @typedef {Object} Ionic~Router
 * @property {RouterPush} push
 * @property {RouterBack} back
 */

/**
 * @return {Router}
 */
function router() {
  // noinspection JSValidateTypes
  return document.querySelector('ion-router');
}
