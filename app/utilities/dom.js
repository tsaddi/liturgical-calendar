/**
 * @callback ionItemSlidingCallback
 * @param {number} index
 */

/**
 * @param {HTMLElement} elm
 */
function clickRadio(elm) {
  const radioElm = elm.querySelector('ion-radio');
  if (radioElm)
    radioElm.click();
}

/**
 * @param {Element} component
 * @param {Element} elm
 * @param {string} selector
 * @param {boolean} hidden
 * @param {ionItemSlidingCallback} callback
 */
function initIonItemSliding(component, elm, selector, hidden, callback) {
  const editButton = component.querySelector(selector);
  editButton.addEventListener('click', async function() {
    hidden = !hidden;
    this.innerText = hidden ? 'Edit' : 'Done';
    // noinspection JSUnresolvedFunction closeSlidingItems() is a method on ion-list
    elm.closeSlidingItems();
    elm.querySelectorAll('ion-icon.delete')
      .forEach(i => i.hidden = hidden);
  });
  editButton.hidden = !hidden;

  elm.querySelectorAll('ion-icon.delete').forEach(i => {
    i.addEventListener('click', async e => {
      e.preventDefault();
      // noinspection JSUnresolvedFunction open() is a method on ion-item-sliding
      await i.parentElement.parentElement.open();
    });
  });

  elm.querySelectorAll('ion-item-option').forEach((v, i) => {
    v.addEventListener('click', async () => callback.apply(component, [i]))
  });

  elm.querySelectorAll('ion-item-sliding').forEach((v, i) => {
    v.addEventListener('ionSwipe', async () => callback(i))
  });
}

/**
 * Walks up through the DOM tree until it finds
 * a parent element with the given node name.
 * @param {HTMLElement} elm The child element from which to start the search.
 * @param {string} nodeName The node name to match.
 * @return {HTMLElement | null} The first matched element, or null.
 */
function parent(elm, nodeName) {
  if (!elm)
    return null;

  const parentElm = elm.parentElement;
  if (!parentElm)
    return null;

  if (parentElm.nodeName === nodeName)
    return parentElm;

  return parent(parentElm, nodeName);
}

/**
 * Removes all event listeners from a node
 * by replacing it with a cloned copy.
 * @param {Node} elm
 * @return {Node} The new node.
 */
function removeEventListeners(elm) {
  var newElm = elm.cloneNode(true);
  elm.parentNode.replaceChild(newElm, elm);
  return newElm;
}

/**
 * @param {HTMLElement} elm
 */
function showContent(elm) {
  elm.querySelector('#loading').remove();
  elm.querySelectorAll('ion-content>*').forEach(e => e.hidden = false);
}

/**
 * @return {number}
 */
function windowInnerWidthInEms() {
  return window.innerWidth / parseFloat(
    getComputedStyle(
      document.querySelector('body')
    )['font-size']
  );
}

/**
 * Escapes str such that it's safe to include in an HTML string.
 */
const x = function() {
  const div = document.createElement('div');
  return (str) => {
    div.innerText = str;
    return div.innerHTML;
  }
}();
