const DAYS_OF_WEEK = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const colourClasses = {
  'b': 'purple',
  'g': 'green',
  'p': 'purple',
  'r': 'red',
  'k': 'pink',
  'w': 'white'
};

const rankIcons = {
  'weekday': 'ellipse',
  'memorial': 'bookmark',
  'feast': 'bookmarks',
  'solemnity': 'ribbon'
};

/**
 * @param {string} shortName
 * @param {string} rank
 * @return {string}
 */
function longName(shortName, rank) {
  if (rank === 'feast' || rank === 'solemnity')
    return `the ${rank} of ${shortName}`;

  if (startsWithWeekday(shortName))
    return shortName;

  if (startsWithNumber(shortName))
    return `the ${shortName}`;

  if (requiresDefiniteArticle(shortName)) {
    if (rank === 'memorial')
      return `the memorial of the ${shortName}`;
    else
      return  `the ${shortName}`;
  }

  if (rank === 'memorial')
    return `the memorial of ${shortName}`;
  else
    return  shortName;
}

/**
 * @param str
 * @return {boolean} Whether the string starts with the word 'dedication'.
 */
function requiresDefiniteArticle(str) {
  if (str === undefined || str === null)
    return false;

  return /^dedication/i.test(str)
}

/**
 * @param str
 * @return {boolean} Whether the string starts with a number.
 */
function startsWithNumber(str) {
  if (str === undefined || str === null)
    return false;

  return /^[0-9]/.test(str)
}

/**
 * @param str
 * @return {boolean} Whether the string starts with a weekday.
 */
function startsWithWeekday(str) {
  if (str === undefined || str === null)
    return false;

  const firstWord = str.split(' ', 2)[0];
  return DAYS_OF_WEEK.indexOf(firstWord) !== -1;
}
