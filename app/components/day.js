customElements.define('app-day',
  /**
   * @property {string} key Populated automatically from the route parameters.
   * @property {AppData | undefined} dataElm
   * @property {string | undefined} dataSubscriptionId
   */
  class extends Base {
    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    async connectedCallback() {
      this.loading = true;

      this.innerHTML  =  `
        <ion-header>
          <ion-toolbar>
              <ion-buttons slot="start">
                  <ion-back-button text="Calendar"></ion-back-button>
              </ion-buttons>
              <ion-title>${x(formatLongDate(new Date(this.key)))}</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list hidden>
            <ion-radio-group id="dayList">
            </ion-radio-group>
          </ion-list>
          
          <ion-list-header hidden>
            <ion-label>Services</ion-label>
            <ion-button id="servicesEdit" hidden>Edit</ion-button>
          </ion-list-header>
          <ion-list id="services"
                    hidden>
          </ion-list>
          
          <div id="loading">
            <ion-spinner></ion-spinner>
          </div>
        </ion-content>
      `;
    }

    /**
     * @param {AppData~Data} data
     * @return {Promise<void>}
     */
    async render(data) {
      if (!data.days.hasOwnProperty(this.key)) {
        await presentToast(`Day ${this.key} not found`);
        await router().push(`/`);
        return;
      }

      this.renderDayList(data);
      this.renderServices(data.services);

      if (this.loading) {
        showContent(this);
        this.loading = false;
      }
    }

    /**
     * @param {AppData~Data} data
     */
    renderDayList(data) {
      let content = `
        <ion-item button
                  detail="false">
          <ion-radio slot="start"
                     value="${x(data.days[this.key].shortName)}"
                     class="radio-circle radio-circle-${x(colourClasses[data.days[this.key].colour])}"></ion-radio>
          <ion-label>${data.days[this.key].shortName}</ion-label>
        </ion-item>
      `;

      this.day = data.days[this.key];
      /** @type {Object.<string, AppData~Memorial>} */
      this.optionalMemorials = {};
      for (const memorial of data.days[this.key].optionalMemorials) {
        this.optionalMemorials[memorial.shortName] = memorial;
        content += `
          <ion-item button
                    detail="false">
            <ion-radio slot="start"
                       value="${memorial.shortName}"
                       class="radio-circle radio-circle-${x(colourClasses[memorial.colour])}"></ion-radio>
            <ion-label>${memorial.shortName}</ion-label>
          </ion-item>
        `;
      }

      content += `
        <ion-item button
                  detail="false">
          <ion-radio slot="start"
                     value=""
                     class="radio-circle white"></ion-radio>
          <ion-label>Custom</ion-label>
          <ion-button slot="end"
                      fill="clear"
                      size="small"
                      href="#/day/${x(this.key)}/custom">
            <ion-icon slot="icon-only" name='information-circle-outline'></ion-icon>
          </ion-button>
        </ion-item>
      `;

      const dayListElm = this.querySelector('#dayList');
      dayListElm.innerHTML = content;

      let initialValue;
      if (data.selection[this.key]) {
        if (this.equals(this.day, data.selection[this.key]) || this.day.optionalMemorials.reduce(
          (p, v) => p || this.equals(v, data.selection[this.key]),
          false
        )) {
          initialValue = data.selection[this.key].shortName;
        } else {
          initialValue = '';
          this.setCustomSelection(data.selection[this.key]);
        }
      } else {
        initialValue = data.days[this.key].shortName;
      }

      dayListElm.value = initialValue;

      this.initDayList(initialValue);
    }

    /**
     * @param {Object.<string, AppData~Service>} services
     */
    renderServices(services) {
      if (!services.hasOwnProperty(this.key))
        return;

      let content = '';

      for (const service of services[this.key].services) {
        content += `
          <ion-item-sliding>
            <ion-item href="#/day/${this.key}/service/${x(encodeURIComponent(service.hash))}"
                      detail="true">
              <ion-icon slot="start"
                        name="remove-circle"
                        color="danger"
                        class="delete"
                        hidden></ion-icon>
                      
              <ion-icon slot="start"
                        ${service.cancelled ? 'name="ban" color="danger"' : (service.streamed ? 'name="videocam" color="tertiary"' : 'name="videocam-off-outline"')}></ion-icon>
                      
              <ion-label>
                ${x(formatTime(service.start))} ${x(service.title)}
                ${service.cancelled ? '<ion-text color="danger" class="ion-text-uppercase ion-padding-start"><small><span class="sr-only">(</span>cancelled<span class="sr-only">)</span></small></ion-text>' : ''}
                <ion-note color="medium">
                  <p>${x(service.location)}</p>
                </ion-note>
              </ion-label>
            </ion-item>
        
            <ion-item-options side="end">
              <ion-item-option color="danger" expandable>
                Delete
              </ion-item-option>
            </ion-item-options>
          </ion-item-sliding>
        `;
      }

      content += `
        <ion-item id="add-service"
                  button
                  detail="true">
          <ion-label>
            Add Service
          </ion-label>
        </ion-item>
      `;

      const servicesElm = this.querySelector('#services');
      servicesElm.innerHTML = content;

      const addElm = this.querySelector('#add-service');
      addElm.addEventListener('click', () => {
        const modalElement = document.createElement('ion-modal');
        modalElement.component = 'app-modal';
        modalElement.presentingElement = document.querySelector('#mainNav');
        modalElement.componentProps = {
          root: document.createElement('app-service'),
          rootParams: { 'key': this.key }
        };

        document.body.appendChild(modalElement);
        // noinspection JSUnresolvedFunction
        return modalElement.present();
      });

      initIonItemSliding(
        this,
        servicesElm,
        '#servicesEdit',
        services[this.key].services.length,
        (index) => this.deleteService(services[this.key].services[index])
      );
    }

    /**
     * @param {string} initialValue
     */
    initDayList(initialValue) {
      const self = this;
      let lastValue = initialValue;
      this.querySelectorAll('#dayList ion-item')
        .forEach(i => {
          i.addEventListener('click', async function(e) {
            clickRadio(this);

            // noinspection JSCheckFunctionSignatures
            const group = parent(e.target, 'ION-RADIO-GROUP');
            const value = group.value;
            if (value === lastValue)
              return;

            lastValue = value;
            if (value === '') {
              await router().push(`/day/${self.key}/custom`);
            } else {
              const selection = self.optionalMemorials[value]
                ? self.optionalMemorials[value]
                : self.day;
              await self.dataElm.updateSelection(self.key, selection);
            }
          });
        });
    }

    /**
     * @param {AppData~Service} service
     * @return {Promise<void>}
     */
    async deleteService(service) {
      console.log(`Deleting ${service.hash}.`);
      await this.dataElm.deleteService(this.key, service.hash);
    }

    /**
     * @param {{ shortName: {string}, colour: {string}, rank: {string} }} a
     * @param {{ shortName: {string}, colour: {string}, rank: {string} }} b
     */
    equals (a, b) {
      return a.shortName === b.shortName && a.colour === b.colour && a.rank === b.rank;
    }

    /**
     * @param {AppData~Selection} selection
     */
    setCustomSelection(selection) {
      this.querySelector('#dayList > ion-item:last-child > ion-label').innerHTML = `
        Custom
        <ion-note color="medium">
          <p>${selection.shortName}</p>
        </ion-note>
      `;

      const classList = this.querySelector('#dayList > ion-item:last-child > ion-radio').classList;

      classList.remove('purple');
      classList.remove('green');
      classList.remove('red');
      classList.remove('pink');
      classList.remove('white');

      classList.add(colourClasses[selection.colour]);
    }
  }
);
