customElements.define('app-main', class extends HTMLElement {
  constructor() {
    super();
    this.watchForDarkTheme();
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Invoked each time the custom element is appended
   * into a document-connected element. This will happen
   * each time the node is moved, and may happen before
   * the element's contents have been fully parsed.
   */
  connectedCallback() {
    this.innerHTML = `
      <ion-app>
          <ion-router>
              <ion-route url="/" component="app-home"></ion-route>
              <ion-route url="/day/:key" component="app-day"></ion-route>
              <ion-route url="/day/:key/service/:hash" component="app-service"></ion-route>
              <ion-route url="/day/:key/service/:hash/location" component="app-location"></ion-route>
              <ion-route url="/day/:key/service/:hash/scene/:scene" component="app-scene"></ion-route>
              <ion-route url="/day/:key/custom" component="app-custom"></ion-route>
          </ion-router>
          <app-data></app-data>
          <ion-nav id="mainNav"></ion-nav>
      </ion-app>
    `;
  }

  /**
   * @param {boolean} shouldAdd
   */
  toggleDarkTheme(shouldAdd) {
    document.body.classList.toggle('dark', shouldAdd);
  }

  watchForDarkTheme() {
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    this.toggleDarkTheme(prefersDark.matches);
    // noinspection JSDeprecatedSymbols
    prefersDark.addListener((mediaQuery) => this.toggleDarkTheme(mediaQuery.matches));
  }
});
