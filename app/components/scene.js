customElements.define('app-scene',
  /**
   * @property {string} key Populated automatically from the route parameters.
   * @property {string | undefined} hash Populated automatically from the route parameters.
   * @property {AppData | undefined} dataElm
   * @property {string | undefined} dataSubscriptionId
   */
  class extends Base {
    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    async connectedCallback() {
      this.loading = true;

      this.innerHTML  =  `
        <ion-header>
          <ion-toolbar>
              <ion-buttons slot="start">
                  <ion-back-button text="Back"></ion-back-button>
              </ion-buttons>
              <ion-title>Scene</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list hidden>
          </ion-list>
          
          <div id="loading">
            <ion-spinner></ion-spinner>
          </div>
        </ion-content>
      `;
    }
  }
);
