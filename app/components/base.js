/**
 * @property {AppData | undefined} dataElm
 * @property {string | undefined} dataSubscriptionId
 */
class Base extends HTMLElement {
  constructor() {
    super();

    this.addEventListener('ionViewWillEnter', () => this.willEnter());
    this.addEventListener('ionViewWillLeave', () => this.willLeave());
  }

  /**
   * Override this method in subclasses.
   */
  render() {
  }

  willEnter() {
    if (this.dataSubscriptionId === undefined) {
      this.dataElm = document.querySelector('app-data')
      this.dataSubscriptionId = this.dataElm.subscribe(data => this.render(data));
    }
  }

  willLeave() {
    if (this.dataSubscriptionId !== undefined) {
      this.dataElm.unsubscribe(this.dataSubscriptionId);
      this.dataSubscriptionId = undefined;
    }
  }
}
