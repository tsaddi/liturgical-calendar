/**
 * @callback AppService~changeCallback
 * @param {HTMLInputElement} elm
 */

const SERVICE_MAX_DURATION_MINUTES = 230;

customElements.define('app-service',
  /**
   * @property {string} key Populated automatically from the route parameters.
   * @property {string | undefined} hash Populated automatically from the route parameters.
   * @property {AppData | undefined} dataElm
   * @property {string | undefined} dataSubscriptionId
   * @property {AppData~Service | undefined} service
   */
  class AppService extends Base {
    /**
     * @type {AppData~Service}
     */
    defaults = {
      title: 'Mass',
      location: 'St Joseph\'s',
      start: '19:00',
      duration: 60,
      streamed: true,
      cancelled: false,
      scenes: null
    }

    form = {
      title: () => this.querySelector('#serviceTitle'),
      location: () => this.querySelector('#serviceLocationLabel'),
      start: () => this.querySelector('#serviceStart'),
      duration: () => this.querySelector('#serviceDuration'),
      streamed: () => this.querySelector('#serviceStreamed'),
      cancelled: () => this.querySelector('#serviceCancelled'),
    }

    constructor() {
      super();
      this.DEBOUNCE_DELAY = 250;
      this.service = this.defaults;
    }

    /**
     * @return {Date}
     */
    get keyAsDate() {
      return new Date(this.key);
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    async connectedCallback() {
      this.loading = true;

      // TODO: Implement scene changes in FacebookLive.exe,
      //       then implement them here.
      this.innerHTML = `
        <ion-header>
          <ion-toolbar>
            ${this.hash === undefined
              ? `
                <ion-buttons slot="primary">
                  <ion-button strong id="service-done">Done</ion-button>
                </ion-buttons>
                <ion-buttons slot="secondary">
                  <ion-button id="service-cancel">Cancel</ion-button>
                </ion-buttons>
              `
              : `
                <ion-buttons slot="start">
                  <ion-back-button text="${x(
                    formatShortDate(this.keyAsDate)
                    + (windowInnerWidthInEms() > 33
                       ? ' ' + formatMonth(this.keyAsDate)
                       : '')
                  )}"></ion-back-button>
                </ion-buttons>
              `}
            <ion-title>Service</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list hidden>
            <ion-item>
              <ion-label position="fixed">Title</ion-label>
              <ion-input id="serviceTitle"
                         placeholder="Title"
                         type="text"
                         required></ion-input>
            </ion-item>
            <ion-item id="serviceLocationButton"
                      button
                      detail="true">
              <ion-label>Location</ion-label>
              <ion-text slot="end"
                        color="medium"
                        id="serviceLocationLabel"></ion-text>
            </ion-item>
            <ion-item>
              <ion-label position="fixed">Start</ion-label>
              <ion-input id="serviceStart"
                         type="time"
                         required></ion-input>
            </ion-item>
            <ion-item>
              <ion-label position="fixed">Duration</ion-label>
              <ion-input id="serviceDuration"
                         type="text"
                         pattern="[0-9]*"
                         inputmode="numeric"
                         required></ion-input>
              <ion-text slot="end" color="medium">minutes</ion-text>
            </ion-item>
            <ion-item>
              <ion-label>Live stream</ion-label>
              <ion-toggle id="serviceStreamed"
                          slot="end" 
                          checked></ion-toggle>
            </ion-item>
            <ion-item>
              <ion-label>Cancelled</ion-label>
              <ion-toggle id="serviceCancelled"
                          slot="end"
                          color="danger"></ion-toggle>
            </ion-item>
          </ion-list>
          
          <div id="scenes" style="display:none !important"></div>
          
          <div id="loading">
            <ion-spinner></ion-spinner>
          </div>
        </ion-content>
      `;

      const cancelElm = this.querySelector('#service-cancel');
      if (cancelElm)
        cancelElm.addEventListener('click', () => this.dismiss());

      const doneElm = this.querySelector('#service-done');
      if (doneElm)
        doneElm.addEventListener('click', () => this.addService());

      this.form.title().addEventListener('input', debounce(this.changeThingFactory(elm =>
        this.service.title = elm.value.trim()
      ), this.DEBOUNCE_DELAY));

      this.form.start().addEventListener('input', debounce(this.changeThingFactory(elm =>
        this.service.start = elm.value.trim()
      ), this.DEBOUNCE_DELAY));

      this.form.duration().addEventListener('input', debounce(this.changeThingFactory(elm =>
        this.service.duration = parseInt(elm.value.trim(), 10)
      ), this.DEBOUNCE_DELAY));

      this.form.streamed().addEventListener('ionChange', debounce(this.changeThingFactory(elm => {
        this.service.streamed = elm.checked;
        this.querySelector('#scenes').hidden = !elm.checked;
      }), this.DEBOUNCE_DELAY));

      this.form.cancelled().addEventListener('ionChange', debounce(this.changeThingFactory(elm => {
        this.service.cancelled = elm.checked;
      }), this.DEBOUNCE_DELAY));

      this.querySelector('#serviceLocationButton').addEventListener('click', () => {
        if (this.hash) {
          router().push(`/day/${this.key}/service/${this.hash}/location`);
        } else {
          const locationComponent = document.createElement('app-location');
          locationComponent.key = this.key;
          locationComponent.location = this.form.location().innerText;
          locationComponent.callback = location => this.service.location = location;

          this.closest('ion-nav').push(locationComponent);
        }
      });
    }

    async addService() {
      const data = {
        title: this.form.title().value.trim(),
        location: this.form.location().innerText.trim(),
        start: this.form.start().value.trim(),
        duration: this.form.duration().value,
        streamed: this.form.streamed().checked,
        cancelled: this.form.cancelled().checked,
        scenes: [{ time: 0, scene: 'Sanctuary' }]
      }

      if (!await this.validate(data))
        return;

      await this.dataElm.addService(this.key, data);

      await this.dismiss();
    }

    /**
     * @param {AppService~changeCallback} callback
     * @return {function(Event): Promise<void>}
     */
    changeThingFactory(callback) {
      return /** @param {Event} e*/ async (e) => {
        if (!this.hash || !this.service)
          return;

        // noinspection JSValidateTypes The event target is an HTMLInputElement.
        /** @type {HTMLInputElement}*/
        const inputElm = e.target;

        callback(inputElm);

        /** @type {AppData~Service} */
        const newDetails = await this.dataElm.updateService(this.key, this.hash, this.service, false);
        this.hash = newDetails.hash;
        this.dataElm.publish();
      }
    }

    /**
     * @param {number} index
     * @return {Promise<void>}
     */
    async deleteScene(index) {
      // TODO: Delete the scene change (i.e. make an API call)
      console.log(`Deleting ${index}.`);
    }

    async dismiss() {
      await this.closest('ion-modal').dismiss({
        'dismissed': true
      });
    }

    /**
     * @param {AppData~Data} data
     * @return {Promise<void>}
     */
    async render(data) {
      if (!data.days.hasOwnProperty(this.key)) {
        await presentToast(`Day ${this.key} not found`);
        await router().push('/');
        return;
      }

      if (this.hash !== undefined && data.services[this.key]) {
        let service = undefined;
        for (var i = 0, j = data.services[this.key].services.length; i < j; i++) {
          if (data.services[this.key].services[i].hash === this.hash) {
            service = data.services[this.key].services[i];
            break;
          }
        }
        if (service) {
          this.service = service;
        } else {
          console.info(this.hash, data.services[this.key].services);
          await presentToast(`Service ${this.hash} not found`);
          await router().push(`/day/${this.key}`);
          return;
        }
      }

      this.renderService();

      if (this.loading) {
        showContent(this);
        this.loading = false;
      }
    }

    renderService() {
      if (!this.service)
        return;

      this.form.title().value = this.service.title;
      this.form.location().innerText = this.service.location;
      this.form.start().value = padTime(this.service.start);
      this.form.duration().value = this.service.duration;
      this.form.streamed().checked = this.service.streamed;
      this.form.cancelled().checked = this.service.cancelled;

      this.renderScenes();
    }

    renderScenes() {
      if (!this.service.scenes)
        return;

      let content = '';

      let index = 0;
      for (const scene of this.service.scenes) {
        index++;
        if (scene.time === 0) {
          content += `
          <ion-item href="#/day/${x(this.key)}/service/${x(this.hash)}/scene/${index}"
                    detail="true">
            <ion-label>
              ${x(scene.scene)}
              <ion-note>
                <p>Start</p>
              </ion-note>
            </ion-label>
          </ion-item>`;
        } else {
          content += `
          <ion-item-sliding>
            <ion-item href="#/day/${x(this.key)}/service/${x(this.hash)}/scene/${x(index)}"
                      detail="true">
              <ion-icon slot="start"
                        name="remove-circle"
                        color="danger"
                        class="delete"
                        hidden></ion-icon>
              <ion-label>
                ${x(scene.scene)}
                <ion-note>
                  <p>${x(scene.time)} minutes</p>
                </ion-note>
              </ion-label>
            </ion-item>

            <ion-item-options side="end">
              <ion-item-option color="danger" expandable>
                Delete
              </ion-item-option>
            </ion-item-options>
          </ion-item-sliding>`;
        }
      }

      content += `
        <ion-item href="#/day/${x(this.key)}/service/${x(this.hash)}/scene"
                  detail="true">
          <ion-label>
            Add Scene Change
          </ion-label>
        </ion-item>`;

      content = `
        <ion-list-header>
          <ion-label>Scene Changes</ion-label>
          <ion-button id="scenesEdit" hidden>Edit</ion-button>
        </ion-list-header>
        <ion-list id="scenes">${content}</ion-list>
      `;

      const scenesElm = this.querySelector('#scenes');
      scenesElm.innerHTML = content;

      initIonItemSliding(
        this,
        scenesElm,
        '#scenesEdit',
        this.service.scenes.length > 1,
        this.deleteScene
      );
    }

    /**
     * @param {AppData~Service} data
     * @return {boolean}
     */
    async validate(data) {
      const errors = [];

      if (!data.title)
        errors.push(`Please provide a title for this service.`);

      if (!data.location)
        errors.push(`Please select a location.`);

      if (!data.start)
        errors.push(`Please choose a start time for this service.`);

      if (!data.duration)
        errors.push(`Please provide a duration.`);

      if (data.streamed && data.duration > SERVICE_MAX_DURATION_MINUTES)
        errors.push(`The duration of a live-streamed service cannot be longer than ${SERVICE_MAX_DURATION_MINUTES} minutes.`);

      if (errors.length) {
        await presentAlert(['OK'], errors.join('<br><br>'));
        return false;
      }

      return true;
    }
  }
);
