customElements.define('app-defaults',
  /**
   * @property {string | undefined} dataSubscriptionId
   */
  class extends Base {
    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    connectedCallback() {
      this.loading = true;

      this.innerHTML  =  `
        <ion-header>
          <ion-toolbar>
            <ion-title>Defaults</ion-title>
            <ion-buttons slot="start">
              <ion-button id="defaults-close">
                <ion-icon slot="icon-only" name="close"></ion-icon>
              </ion-button>
            </ion-buttons>
            <ion-buttons slot="end">
              <ion-button id="defaults-edit">
                Edit
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
        <ion-content class="ion-padding">
          <ion-list hidden>
          </ion-list>
          
          <div id="loading">
            <ion-spinner></ion-spinner>
          </div>
        </ion-content>
      `;

      this.querySelector('#defaults-close')
        .addEventListener('click', () => this.dismiss())
    }

    async dismiss() {
      await this.closest('ion-modal').dismiss({
        'dismissed': true
      });
    }

    /**
     * @param {AppData~Data} data
     */
    render(data) {
    }
  }
);
