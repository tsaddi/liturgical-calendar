customElements.define('app-home',
  /**
   * @property {AppData | undefined} dataElm
   * @property {string | undefined} dataSubscriptionId
   */
  class extends Base {
    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    connectedCallback() {
      this.loading = true;

      // TODO: Implement the screen to edit the default
      //       services for each day of the week.
      this.innerHTML  =  `
        <ion-header>
          <ion-toolbar>
            <ion-title>Liturgical Calendar</ion-title>
            <ion-buttons slot="primary" style="display: none !important">
              <ion-button id="defaults">
                <ion-icon slot="icon-only"
                          name="albums-outline"></ion-icon>
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list>
          </ion-list>
          
          <div id="loading">
            <ion-spinner></ion-spinner>
          </div>
        </ion-content>
      `;

      this.querySelector('#defaults').addEventListener('click', () => {
        const modalElement = document.createElement('ion-modal');
        modalElement.component = 'app-defaults';
        modalElement.swipeToClose = true;
        modalElement.presentingElement = document.querySelector('#mainNav');

        document.body.appendChild(modalElement);
        // noinspection JSUnresolvedFunction
        return modalElement.present();
      });
    }

    /**
     * @param {AppData~Data} data
     */
    render(data) {
      let content = '';

      for (const key of Object.keys(data.days)) {
        const date = new Date(data.days[key].date);

        const selection = data.selection[key] ? data.selection[key] : data.days[key];

        content += `
          <ion-item button
                    href="#/day/${x(data.days[key].date)}">
            <ion-icon slot="start"
                      name='${x(rankIcons[selection.rank])}'
                      class="${x(colourClasses[selection.colour])}"></ion-icon>
            <ion-label>
              <ion-note color="medium">
                <p>${x(formatShortDate(date))}</p>
              </ion-note>
              ${selection.shortName}
            </ion-label>
            <ion-badge ${x(data.days[key].optionalMemorials.length ? '' : 'hidden')}
                       color="medium"
                       slot="end">
                ${x(data.days[key].optionalMemorials.length + 1)}
            </ion-badge>
          </ion-item>
        `;

        if (this.loading) {
          showContent(this);
          this.loading = false;
        }
      }

      this.querySelector('ion-list').innerHTML = content;
    }
  }
);
