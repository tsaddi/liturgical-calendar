customElements.define('app-modal',
  /**
   * @property {HTMLElement} root Populated by the calling component.
   * @property {object} rootParams Populated by the calling component.
   */
  class extends HTMLElement {
    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    connectedCallback() {
      this.innerHTML = `<ion-nav id="modalNav"></ion-nav>`;

      const nav = this.querySelector('#modalNav');
      nav.root = this.root;
      nav.rootParams = this.rootParams;
    }
  }
);
