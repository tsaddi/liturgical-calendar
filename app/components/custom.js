customElements.define('app-custom',
  /**
   * @property {string} key Populated automatically from the route parameters.
   * @property {AppData | undefined} dataElm
   * @property {string | undefined} dataSubscriptionId
   */
  class extends Base {
    constructor() {
      super();
      this.DEBOUNCE_DELAY = 250;
    }

    /**
     * @return {Date}
     */
    get keyAsDate() {
      return new Date(this.key);
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    async connectedCallback() {
      this.loading = true;

      this.innerHTML  =  `
        <ion-header>
          <ion-toolbar>
              <ion-buttons slot="start">
                  <ion-back-button text="${x(formatShortDate(this.keyAsDate) + (windowInnerWidthInEms() > 33 ? ' ' + formatMonth(this.keyAsDate) : ''))}"></ion-back-button>
              </ion-buttons>
              <ion-title>Custom</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list hidden>
              <ion-item>
                <ion-input id="customName"
                           placeholder="Name"
                           type="text"
                           required></ion-input>
            </ion-item>
          </ion-list>
  
          <ion-list-header hidden>
            <ion-label>Colour</ion-label>
          </ion-list-header>
          <ion-segment id="colourList"
                       value="w"
                       hidden>
            <ion-segment-button value="g">
                <ion-icon name='ellipse' class="green"></ion-icon>
                <ion-label>Green</ion-label>
            </ion-segment-button>
            <ion-segment-button value="r">
                <ion-icon name='ellipse' class="red"></ion-icon>
                <ion-label>Red</ion-label>
            </ion-segment-button>
            <ion-segment-button value="p">
                <ion-icon name='ellipse' class="purple"></ion-icon>
                <ion-label>Purple</ion-label>
            </ion-segment-button>
            <ion-segment-button value="k">
                <ion-icon name='ellipse' class="pink"></ion-icon>
                <ion-label>Rose</ion-label>
            </ion-segment-button>
            <ion-segment-button value="w">
                <ion-icon name='ellipse' class="white"></ion-icon>
                <ion-label>White</ion-label>
            </ion-segment-button>
          </ion-segment>
  
          <ion-list-header hidden>
            <ion-label>Rank</ion-label>
          </ion-list-header>
          <ion-list hidden>
            <ion-radio-group id="rankList"
                             value="memorial">
              <ion-item button
                        detail="false"
                        onclick="clickRadio(this)">
                  <ion-icon slot="start"
                            name='ellipse'
                            color="secondary"></ion-icon>
                  <ion-label>Weekday</ion-label>
                  <ion-radio slot="end" value="weekday"></ion-radio>
              </ion-item>
              <ion-item button
                        detail="false"
                        onclick="clickRadio(this)">
                  <ion-icon slot="start"
                            name='bookmark'
                            color="secondary"></ion-icon>
                  <ion-label>Memorial</ion-label>
                  <ion-radio slot="end" value="memorial"></ion-radio>
              </ion-item>
              <ion-item button
                        detail="false"
                        onclick="clickRadio(this)">
                  <ion-icon slot="start"
                            name='bookmarks'
                            color="secondary"></ion-icon>
                  <ion-label>Feast</ion-label>
                  <ion-radio slot="end" value="feast"></ion-radio>
              </ion-item>
              <ion-item button
                        detail="false"
                        onclick="clickRadio(this)">
                  <ion-icon slot="start"
                            name='ribbon'
                            color="secondary"></ion-icon>
                  <ion-label>Solemnity</ion-label>
                  <ion-radio slot="end" value="solemnity"></ion-radio>
              </ion-item>
            </ion-radio-group>
          </ion-list>
          
          <div id="loading">
            <ion-spinner></ion-spinner>
          </div>
        </ion-content>
      `;

      this.querySelector('ion-input').addEventListener(
        'input',
        debounce(this.changeName, this.DEBOUNCE_DELAY)
      );
      this.querySelector('#colourList').addEventListener('ionChange', this.changeColour);
      this.querySelector('#rankList').addEventListener('ionChange', this.changeRank);
    }

    /**
     * @param {AppData~Data} data
     * @return {Promise<void>}
     */
    async render(data) {
      if (!data.days.hasOwnProperty(this.key)) {
        await presentToast(`Day ${this.key} not found`);
        await router().push(`/`);
        return;
      }

      if (!data.selection.hasOwnProperty(this.key))
        return;

      this.selection = data.selection[this.key];

      if (this.selection.shortName)
        this.querySelector('#customName').value = this.selection.shortName;

      if (this.selection.colour)
        this.querySelector('#colourList').value = this.selection.colour;

      if (this.selection.rank)
        this.querySelector('#rankList').value = this.selection.rank;

      if (this.loading) {
        showContent(this);
        this.loading = false;
      }
    }

    changeColour = async (e) => {
      this.selection.colour = e.detail.value;
      await this.dataElm.updateSelection(this.key, this.selection);
    }

    /**
     * @param {InputEvent} e
     */
    changeName = async (e) => {
      // noinspection JSValidateTypes The event target is an HTMLInputElement.
      /** @type {HTMLInputElement} */
      const inputElm = e.target;

      this.selection.shortName = inputElm.value;
      this.selection.longName = longName(inputElm.value, this.selection.rank);

      await this.dataElm.updateSelection(this.key, this.selection);
    }

    changeRank = async (e) => {
      this.selection.rank = e.detail.value;
      this.selection.longName = longName(this.selection.shortName, this.selection.rank);

      await this.dataElm.updateSelection(this.key, this.selection);
    }
  }
);
