/**
 * @callback LocationCallback
 * @param {string} location
 */

customElements.define('app-location',
  /**
   * @property {string} key Populated automatically from the route parameters.
   * @property {string | undefined} hash Populated automatically from the route parameters.
   * @property {string | undefined} location Populated by the calling component (if creating a service).
   * @property {LocationCallback | undefined} callback Populated by the calling component (if creating a service).
   * @property {AppData | undefined} dataElm
   * @property {string | undefined} dataSubscriptionId
   */
  class extends Base {
    // noinspection JSUnusedGlobalSymbols
    /**
     * Invoked each time the custom element is appended
     * into a document-connected element. This will happen
     * each time the node is moved, and may happen before
     * the element's contents have been fully parsed.
     */
    async connectedCallback() {
      this.loading = true;

      this.innerHTML  =  `
        <ion-header>
          <ion-toolbar>
              <ion-buttons slot="start">
                  <ion-back-button text="Back"></ion-back-button>
              </ion-buttons>
              <ion-title>Location</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <ion-list id="locationList" hidden>
            <ion-radio-group id="locations">
            </ion-radio-group>
          </ion-list>
          
          <div id="loading">
            <ion-spinner></ion-spinner>
          </div>
        </ion-content>
      `;
    }

    /**
     * @param {AppData~Data} data
     * @return {Promise<void>}
     */
    async render(data) {
      if (!data.days.hasOwnProperty(this.key)) {
        await presentToast(`Day ${this.key} not found`);
        await router().push('/');
        return;
      }

      /** @type {AppData~Service | undefined} */
      let service = undefined;
      if (this.hash === undefined) {
        // We're adding a service
        this.renderLocations(data.locations, undefined, this.location);
      } else {
        for (var i = 0, j = data.services[this.key].services.length; i < j; i++) {
          if (data.services[this.key].services[i].hash === this.hash) {
            service = data.services[this.key].services[i];
            break;
          }
        }
        if (!service) {
          await presentToast(`Service ${this.hash} not found`);
          await router().push(`/day/${this.key}`);
          return;
        }

        this.renderLocations(data.locations, service);
      }

      if (this.loading) {
        showContent(this);
        this.loading = false;
      }
    }

    /**
     * @param {Object<string, AppData~Location>} locations
     * @param {AppData~Service | undefined} service
     * @param {string} [location]
     */
    renderLocations(locations, service, location) {
      let content = '';

      for (const key in locations) {
        if (!locations.hasOwnProperty(key))
          continue;

        const location = locations[key];

        content += `
          <ion-item button>
            <ion-label>${x(location.name)}</ion-label>
            <ion-radio slot="end" value="${x(location.name)}"></ion-radio>
          </ion-item>`;
      }

      var radioGroup = this.querySelector('#locations');
      radioGroup.innerHTML = content;
      radioGroup.value = this.hash ? service.location : location;

      this.initLocation(locations, service, location);
    }

    /**
     * @param {Object<string, AppData~Location>} locations
     * @param {AppData~Service | undefined} service
     * @param {string?} location
     */
    initLocation(locations, service, location) {
      const self = this;
      let lastValue = this.hash ? service.location : location;
      this.querySelectorAll('#locationList ion-item')
        .forEach(i => {
          i.addEventListener('click', async function(e) {
            clickRadio(this);

            // noinspection JSCheckFunctionSignatures
            const group = parent(e.target, 'ION-RADIO-GROUP');
            const value = group.value;
            if (value === lastValue)
              return;

            lastValue = value;

            if (self.hash === undefined) {
              // We're adding a service
              if (self.callback)
                self.callback(value);

              self.closest('ion-nav').pop();
            } else {
              if (service === undefined)
                throw new Error('Both hash and service are undefined.');

              service.location = value;
              /** @type {AppData~Service} */
              const newDetails = await self.dataElm.updateService(self.key, self.hash, service, false);
              await router().push(`/day/${self.key}/service/${newDetails.hash}`);
              self.dataElm.publish();
            }
          });
        });
    }
  }
);
