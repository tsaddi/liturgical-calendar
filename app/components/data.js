/**
 * @typedef {Object} AppData~Data
 * @property {Object<string, AppData~Day>} days
 * @property {Object<string, AppData~Location>} locations
 * @property {Object<string, AppData~Selection>} selection
 * @property {Object<string, AppData~Service>} services
 */

/**
 * @typedef {Object} AppData~DataUpdate
 * @property {Object<string, AppData~Day>} [days]
 * @property {Object<string, AppData~Selection>} [selection]
 */

/**
 * @typedef {Object} AppData~Day
 * @extends {AppData~Selection}
 * @property {AppData~Memorial[]} days.optionalMemorials
 */

/**
 * @typedef {Object} AppData~Location
 * @property {string} name
 * @property {boolean} streamByDefault
 * @property {boolean} automaticStreaming
 */

/**
 * @typedef {Object} AppData~Memorial
 * @property {string} shortName
 * @property {string} longName
 * @property {string} rank
 * @property {string} colour
 */

/**
 * @typedef {Object} AppData~Scene
 * @property {number} time
 * @property {string} scene
 */

/**
 * @typedef {Object} AppData~Selection
 * @property {string} shortName
 * @property {string} longName
 * @property {string} rank
 * @property {string} colour
 * @property {string} date
 * @property {string} psalterWeekNumber
 */

/**
 * @typedef {Object} AppData~Service
 * @property {string} title
 * @property {string} location
 * @property {string} start
 * @property {number} duration
 * @property {boolean} streamed
 * @property {boolean} cancelled
 * @property {AppData~Scene[]} [scenes]
 * @property {string} [hash]
 */

/**
 * @callback AppData~subscribeCallback
 * @param {AppData~Data} data
 */

class AppData extends HTMLElement {
  constructor() {
    super();

    /** @type {AppData~Data}  */
    this.data = {
      days: {},
      locations: {},
      selection: {},
      services: {}
    };
    this.dataLoaded = false;

    /**
     *
     * @type {Object.<string, AppData~subscribeCallback>}
     */
    this.subscribers = {};
    this.subscriberCount = 0;

    this.init().catch(e => presentToast(`Error initialising AppData: ${e.message}`));
  }

  async init() {
    await this.loadData();
    await this.loadServices();
    await this.loadLocations();
    this.dataLoaded = true;
  }

  /**
   * @param {string} date
   * @param {AppData~Selection} selection
   */
  async updateSelection(date, selection) {
    const response = await fetch(
      `api.php?action=days&date=${date}`,
      {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(selection)
      }
    );
    if (!response)
      throw new Error('Failed to update selection.');

    if (!response.ok)
      throw new Error('Failed to update selection (response status ' + response.status + ').');

    this.data.selection[date] = await response.json();
    this.publish();
  }

  /**
   * @param {string} date
   * @param {AppData~Service} data
   * @return {AppData~Service}
   */
  async addService(date, data) {
    const response = await fetch(
      `api.php?action=services&date=${date}`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
      }
    );
    if (!response)
      throw new Error('Failed to add service.');

    if (!response.ok)
      throw new Error('Failed to add service (response status ' + response.status + ').');

    /** @type {AppData~Service} */
    const newDetails = await response.json();
    this.data.services[date].services.push(newDetails);
    this.publish();

    return newDetails;
  }

  /**
   * @param {string} date
   * @param {string} hash
   * @return {Promise<void>}
   */
  async deleteService(date, hash) {
    const response = await fetch(
      `api.php?action=services&date=${date}&hash=${hash}`,
      { method: 'DELETE' }
    );
    if (!response)
      throw new Error('Failed to delete service.');

    if (!response.ok)
      throw new Error('Failed to delete service (response status ' + response.status + ').');

    let done = false;
    for (var i = 0, j = this.data.services[date].services.length; i < j; i++) {
      if (this.data.services[date].services[i].hash === hash) {
        this.data.services[date].services.splice(i, 1);
        done = true;
        break;
      }
    }
    if (!done)
      throw new Error('Failed to delete service from client-side cache.');

    this.publish();
  }

  /**
   * @param {string} date
   * @param {string} hash
   * @param {AppData~Service} data
   * @param {boolean} [publish=false]
   * @return {Promise<AppData~Service>}
   */
  async updateService(date, hash, data, publish) {
    const response = await fetch(
      `api.php?action=services&date=${date}&hash=${hash}`,
      {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
      }
    );
    if (!response)
      throw new Error('Failed to update service.');

    if (!response.ok)
      throw new Error('Failed to update service (response status ' + response.status + ').');

    /** @type {AppData~Service} */
    const newDetails = await response.json();

    let done = false;
    for (var i = 0, j = this.data.services[date].services.length; i < j; i++) {
      if (this.data.services[date].services[i].hash === hash) {
        this.data.services[date].services.splice(i, 1, newDetails);
        done = true;
        break;
      }
    }
    if (!done)
      throw new Error('Failed to add new service details to client-side cache.');

    if (publish)
      this.publish();

    return newDetails;
  }

  /**
   * @param {AppData~subscribeCallback} callback
   * @returns {string} ID which can be passed into {@link unsubscribe}.
   */
  subscribe(callback) {
    this.subscriberCount++;
    var id = this.subscriberCount.toString();
    this.subscribers[id] = callback;

    if (this.dataLoaded)
      callback(this.data);

    return id;
  }

  /**
   * @param {string} id
   * @returns {boolean}
   */
  unsubscribe(id) {
    if (!this.subscribers.hasOwnProperty(id))
      return false;

    delete this.subscribers[id];
    return  true;
  }

  async loadData() {
    const response = await fetch('api.php?action=days');
    if (!response)
      throw new Error('Failed to get day info.');

    if (!response.ok)
      throw new Error('Failed to get day info (response status ' + response.status + ').');

    /** @type {{ days: AppData~Day[], selection: AppData~Selection[] }} */
    const json = await response.json();
    if (!json.hasOwnProperty('days'))
      throw new Error('Failed to get day info (no `days` property in response).');

    const days = {};
    for (var i = 0, j = json.days.length; i < j; i++)
      days[json.days[i].date] = json.days[i];

    const selection = {};
    for (i = 0, j = json.selection.length; i < j; i++)
      selection[json.selection[i].date] = json.selection[i];

    this.data.days = days;
    this.data.selection = selection;
    this.publish();
  }

  async loadLocations() {
    const response = await fetch('api.php?action=locations');
    if (!response)
      throw new Error('Failed to get locations.');

    if (!response.ok)
      throw new Error('Failed to get day locations (response status ' + response.status + ').');

    /** @type {{ locations: Location[] }} */
    const json = await response.json();
    if (!json.hasOwnProperty('locations'))
      throw new Error('Failed to get locations (no `locations` property in response).');

    const locations = {};
    for (var i = 0, j = json.locations.length; i < j; i++)
      locations[json.locations[i].name] = json.locations[i];

    this.data.locations = locations;
    this.publish();
  }

  async loadServices() {
    const response = await fetch('api.php?action=services');
    if (!response)
      throw new Error('Failed to get services.');

    if (!response.ok)
      throw new Error('Failed to get services (response status ' + response.status + ').');

    /** @type {AppData~Service[][]} */
    const json = await response.json();
    if (!json.hasOwnProperty('services'))
      throw new Error('Failed to get services (no `services` property in response).');

    const services = {};
    for (var i = 0, j = json.services.length; i < j; i++)
      services[json.services[i].date] = json.services[i];

    this.data.services = services;
    this.publish();
  }

  publish() {
    for (const id of Object.keys(this.subscribers))
      this.subscribers[id](this.data);
  }
}

customElements.define('app-data', AppData);
