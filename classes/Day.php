<?php

class Day extends DaySelection implements JsonSerializable
{
	/** @var DayBase[] */
	public $optionalMemorials = [];

	/**
	 * Constructor.
	 * @param DateTime | object $valueOrObject The date (in which case $html and $colour must be given), or an object containing details of the day (in which case $html and $colour will be ignored).
	 * @param string | null $html The entire HTML content of the 'day' property of the Universalis JSONP feed for this day.
	 * @param DayExtras | null $extras Additional information about the day.
	 */
	public function __construct($valueOrObject, $html = null, $extras = null)
	{
		if (is_null($html)) {
			parent::__construct($valueOrObject);

			foreach ($valueOrObject->optionalMemorials as $optionalMemorial)
				$this->optionalMemorials[] = new DayBase($optionalMemorial);

			return;
		}

		parent::__construct($valueOrObject, $html, $extras);

		if (preg_match_all('|&#160;&#160;or ([^<]+)<|', $html, $matches)) {
			foreach ($matches[1] as $match)
				$this->optionalMemorials[] = new DayBase($match, '', $extras);
		}
	}

	/**
	 * Specifies data which should be serialized to JSON.
	 * @return array Data which can be serialized by json_encode.
	 */
	public function jsonSerialize()
	{
		$arr = parent::jsonSerialize();
		$arr['optionalMemorials'] = $this->optionalMemorials;
		return $arr;
	}
}
