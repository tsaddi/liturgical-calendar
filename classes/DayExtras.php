<?php

class DayExtras
{
	/**
	 * @var string[] An associative array of day/memorial names to their liturgical colour
	 * (see the DayBase::COLOUR_* constants).
	 */
	public $colours = [];

	/** @var int|null */
	public $psalterWeekNumber;
}
