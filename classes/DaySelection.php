<?php

class DaySelection extends DayBase implements JsonSerializable
{
	/** @var DateTime */
	public $date;

	/** @var int | null */
	public $psalterWeekNumber;

	/**
	 * Constructor.
	 * @param DateTime | mixed $valueOrObject The date (in which case $html and $extras must be given), or an object containing details of the day (in which case $html and $extras will be ignored).
	 * @param string | null $html The entire HTML content of the 'day' property of the Universalis JSONP feed for this day.
	 * @param DayExtras | null $extras Additional information about the day.
	 */
	public function __construct($valueOrObject, $html = null, $extras = null)
	{
		if (is_null($html)) {
			parent::__construct($valueOrObject);
			$this->date = DateTime::createFromFormat('Y-m-d', $valueOrObject->date);
			$this->psalterWeekNumber = $valueOrObject->psalterWeekNumber;
			return;
		}

		$html = trim($html);
		if (empty($html))
			return null;

		if (!preg_match('|<b>([^<]+)</b>|', $html, $matches))
			return null;

		parent::__construct($matches[1], $html, $extras);
		$this->date = $valueOrObject;
		$this->psalterWeekNumber = $extras->psalterWeekNumber;
	}

	/**
	 * Specifies data which should be serialized to JSON.
	 * @return array Data which can be serialized by json_encode.
	 */
	public function jsonSerialize()
	{
		return [
			'date' => $this->date->format('Y-m-d'),
			'shortName' => $this->shortName,
			'longName' => $this->longName,
			'rank' => $this->rank,
			'colour' => $this->colour,
			'psalterWeekNumber' => $this->psalterWeekNumber
		];
	}
}
