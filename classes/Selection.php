<?php

class Selection {
	/** @var string */
	private $cacheFile;
	/** @var DaySelection[] */
	private $days = [];

	/**
	 * Selection constructor.
	 * @param string $cacheFie
	 */
	public function __construct($cacheFie)
	{
		$this->cacheFile = $cacheFie;
		$this->loadCache();
	}

	/**
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	public function __destruct()
	{
		$this->saveCache();
	}

	/**
	 * @param DateTime $date
	 * @return DaySelection|null
	 */
	public function get_day($date) {
		$dateStr = $date->format('Ymd');
		if (array_key_exists($dateStr, $this->days))
			return $this->days[$dateStr];

		return null;
	}

	/**
	 * @param DateTime $date
	 * @param DaySelection $day
	 */
	public function set_day($date, $day) {
		$dateStr = $date->format('Ymd');
		$this->days[$dateStr] = $day;
	}

	private function loadCache() {
		if (!file_exists($this->cacheFile)) {
			// No cache file yet
			$this->days = [];
			return;
		}

		try {
			$data = json_decode_throws(file_get_contents_non_blocking($this->cacheFile));
		} catch (FileException $e) {
			// Throw away the cache
			$data = [];
		} catch (JsonEncodeDecodeException $e) {
			// Throw away the cache
			$data = [];
		}

		$this->days = [];
		foreach ($data->days as $sateStr => $day)
			$this->days[$sateStr] = new DaySelection($day);

		$this->cleanCache();
	}

	private function cleanCache() {
		$today = new DateTime();
		$today->setTime(0,0);

		foreach ($this->days as $dateStr => $day)
			$this->clean_cache_entry($this->days, $dateStr, $today);
	}

	/**
	 * @param array $arr
	 * @param string $dateStr
	 * @param DateTime $today
	 */
	private function clean_cache_entry(&$arr, $dateStr, $today) {
		try {
			$date = new DateTime($dateStr);
		} catch (Exception $e) {
			// Invalid date, delete this cache entry
			unset($arr[$dateStr]);
			return;
		}

		if ($date < $today) {
			// Date in te past, delete this cache entry
			unset($arr[$dateStr]);
		}
	}

	/**
	 * @throws FileException Could not write file.
	 * @throws JsonEncodeDecodeException Error encoding JSON.
	 */
	private function saveCache() {
		$cache = [
			'days' => $this->days
		];

		file_put_contents_throws($this->cacheFile, json_encode_throws($cache), LOCK_EX);
	}
}
