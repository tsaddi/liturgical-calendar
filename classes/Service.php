<?php

class Service
{
	/** @var string */
	public $title;

	/** @var string */
	public $location;

	/** @var string Start time, in the format hh:mm */
	public $start;

	/** @var int Length of service, in minutes. */
	public $duration;

	/** @var bool Whether this service will be live-streamed. */
	public $streamed;

	/** @var bool Whether this service has been cancelled. */
	public $cancelled;

	/** @var Scene[] Scene changes. */
	public $scenes;

	/** @var string Hash identifying this version of this service. */
	public $hash;

	/**
	 * @param object $obj
	 */
	public function __construct($obj)
	{
		$this->title = $obj->title;
		$this->location = $obj->location;
		$this->start = $obj->start;
		$this->duration = $obj->duration;
		$this->streamed = $obj->streamed;
		$this->cancelled = $obj->cancelled ?? false;

		$this->scenes = [];
		if ($obj->scenes) {
			foreach ($obj->scenes as $scene)
				$this->scenes[] = new Scene($scene);
		}

		$this->hash = $this->calculate_hash();
	}

	/**
	 * @return string
	 */
	private function calculate_hash(): string
	{
		return md5(json_encode([
			'title' => $this->title,
			'location' => $this->location,
			'start' => $this->start,
			'duration' => $this->duration,
			'streamed' => $this->streamed,
			'cancelled' => $this->cancelled,
			'scenes' => $this->scenes
		]));
	}
}
