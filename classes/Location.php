<?php

class Location
{
	/** @var string */
	public $name;

	/** @var bool Whether new services at this location should have streaming selected by default. */
	public $streamByDefault;

	/** @var bool Whether the equipment at this location supports streaming without someone having to manually start and stop it. */
	public $automaticStreaming;

	/**
	 * @param object $obj
	 */
	public function __construct($obj)
	{
		$this->name = strval($obj->name);
		$this->automaticStreaming = boolval($obj->automaticStreaming);
		$this->streamByDefault = boolval($obj->streamByDefault);
	}
}
