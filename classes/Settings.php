<?php

class Settings {
	/** @var string */
	public $selectionCacheFile;

	/** @var string */
	public $servicesCacheFile;

	/** @var string */
	public $logDirectory;

	/** @var Location[] */
	public $locations;

	/** @var Service[][] List of regular services for each day (Sunday is the first day of the week), */
	public $defaultServices;

	/** @var string */
	public $universalisCalendarUrl;

	/** @var string */
	public $universalisUrl;

	/** @var string */
	public $universalisCallback;

	/** @var string */
	public $universalisCacheFile;

	/**
	 * Constructor.
	 * @throws FileException File not found.
	 * @throws JsonEncodeDecodeException Invalid JSON.
	 */
	public function __construct()
	{
		$json = json_decode_throws(file_get_contents_non_blocking(join_paths(APP_ROOT, 'settings.json')));

		$this->logDirectory = $json->logDirectory;

		$this->selectionCacheFile = $json->selectionCacheFile;

		$this->servicesCacheFile = $json->servicesCacheFile;
		$this->locations = $this->parse_locations($json->locations);
		$this->defaultServices = $this->parse_services($json->defaultServices);

		$this->universalisCalendarUrl = $json->universalisCalendarUrl;
		$this->universalisUrl = $json->universalisUrl;
		$this->universalisCallback = $json->universalisCallback;
		$this->universalisCacheFile = $json->universalisCacheFile;
	}

	/**
	 * @param $json
	 * @return Location[]
	 */
	private function parse_locations($json) {
		$locations = [];

		foreach ($json as $location)
			$locations[] = new Location($location);

		return $locations;
	}

	/**
	 * @param $json
	 * @return Service[][]
	 */
	private function parse_services($json) {
		$days = [];

		foreach ($json as $day) {
			$services = [];
			foreach ($day as $service) {
				$services[] = new Service($service);
			}
			$days[] = $services;
		}

		return $days;
	}
}
