<?php

class Scene
{
	/** @var int Time in minutes, from the start of the broadcast, at which this scene will be shown. */
	public $time;

	/** @var string */
	public $scene;

	/**
	 * Scene constructor.
	 * @param object $obj
	 */
	public function __construct($obj)
	{
		$this->time = $obj->time;
		$this->scene = $obj->scene;
	}
}
