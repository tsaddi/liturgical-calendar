<?php

class DayBase
{
	/** @var string[] Sunday is the first day of the week. */
	const DAYS_OF_WEEK = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	/** @var string[] Sunday is the first day of the week. */
	const SPECIAL_PREFIXES = ["Divine", "Easter"];

	/** @var string Violet is the colour of penance, and black is (for most of us) the colour of mourning. One or the other is therefore used in Masses for the Dead and, in particular, on All Souls’ Day. */
	const COLOUR_BLACK = 'b';
	/** @var string The theological virtue of hope is symbolized by the colour green, just as the burning fire of love is symbolized by red. Green is the colour of growing things, and hope, like them, is always new and always fresh. Liturgically, green is the colour of Ordinary Time, the orderly sequence of weeks through the year, a season in which we are being neither single-mindedly penitent (in purple) nor overwhelmingly joyful (in white). */
	const COLOUR_GREEN = 'g';
	/** @var string Violet is a dark colour, ‘the gloomy cast of the mortified, denoting affliction and melancholy’. Liturgically, it is the colour of Advent and Lent, the seasons of penance and preparation. */
	const COLOUR_VIOLET = 'p';
	/** @var string Red is the colour of fire and of blood. Liturgically, it is used to celebrate the fire of the Holy Spirit (for instance, at Pentecost) and the blood of the martyrs. */
	const COLOUR_RED = 'r';
	/**
	 * @var string Rose is a lighter version of violet, because today the penitential violet is mixed with the white of the approaching festival.
	 *   It is part of human nature that we cannot go on being penitent for a long time, or we sink into a settled and insincere gloom rather than working at the definite and active spiritual exercise called penance. The Church knows human nature, and both in Advent and Lent there is a moment where the atmosphere of penance and preparation is brightened by a shaft of light from the glorious season we are preparing ourselves for.
	 *   The third Sunday of Advent tells us ‘Gaudéte, rejoice!’ because the Lord is near and the fourth Sunday of Lent says ‘Lætáre, Ierúsalem, be joyful, Jerusalem, and all who love her!’ because she herself is loved by the Lord. On Gaudete and Laetare Sundays, therefore, the dark penitential violet may be lightened to what the documents call ‘rose’ but most laymen would call ‘pink’.
	 *   This happens where it is traditional, and appropriate, and vestments of this extra colour are available. Otherwise there is nothing wrong in keeping violet as violet. Ultimately the liturgical colours are there to serve us, not we to serve them.
	 */
	const COLOUR_ROSE = 'k';
	/**
	 * @var string White is the colour of heaven. Liturgically, it is used to celebrate feasts of the Lord; Christmas and Easter, the great seasons of the Lord; and the saints. Not that you will always see white in church, because if something more splendid, such as gold, is available, that can and should be used instead. We are, after all, celebrating.
	 *   In the earliest centuries all vestments were white – the white of baptismal purity and of the robes worn by the armies of the redeemed in the Apocalypse, washed white in the blood of the Lamb. As the Church grew secure enough to be able to plan her liturgy, she began to use colour so that our sense of sight could deepen our experience of the mysteries of salvation, just as incense recruits our sense of smell and music that of hearing. Over the centuries various schemes of colour for feasts and seasons were worked out, and it is only as late as the 19th century that they were harmonized into their present form.
	 */
	const COLOUR_WHITE = 'w';

	/** @var string Commemorates an event in the life of Jesus or Mary, or celebrates a saint important to the whole Church or the local community. */
	const RANK_SOLEMNITY = 'solemnity';
	/** @var string Lesser events in the life of Jesus, Mary or an apostle, or for major saints. */
	const RANK_FEAST = 'feast';
	/** @var string The commemoration of a saint of lesser importance. */
	const RANK_MEMORIAL = 'memorial';
	/** @var string A weekday in ordinary time on which no solemnity, feast or memorial happens to be observed. */
	const RANK_WEEKDAY = 'weekday';

	/** @var string */
	public $shortName;

	/** @var string */
	public $longName;

	/** @var string See the self::RANK_* constants. */
	public $rank;

	/** @var string See the self::COLOUR_* constants. */
	public $colour;

	/**
	 * Constructor.
	 * @param string | mixed $valueOrObject The day name (in which case $html and $extras must be given), or an object containing details of the day (in which case $html and $extras will be ignored).
	 * @param string | null $html The entire HTML content of the 'day' property of the Universalis JSONP feed for this day.
	 * @param DayExtras | null $extras Additional information about the day.
	 */
	public function __construct($valueOrObject, $html = null, $extras = null)
	{
		if (is_null($html)) {
			$this->shortName = $valueOrObject->shortName;
			$this->longName = $valueOrObject->longName;
			$this->rank = $valueOrObject->rank;
			$this->colour = $valueOrObject->colour;
			return;
		}

		if (strpos($html, '-&#160;Solemnity') !== FALSE) {
			$this->shortName = $valueOrObject;
			$this->longName = "the solemnity of $valueOrObject";
			$this->rank = self::RANK_SOLEMNITY;
		} elseif (strpos($html, '-&#160;Feast') !== FALSE) {
			$this->shortName = $valueOrObject;
			$this->longName = "the feast of $valueOrObject";
			$this->rank = self::RANK_FEAST;
		} else if ($valueOrObject === 'Saturday memorial of the Blessed Virgin Mary') {
			$this->shortName = $valueOrObject;
			$this->longName = $valueOrObject;
			$this->rank = self::RANK_MEMORIAL;
		} elseif ($this->starts_with_weekday($valueOrObject) || $this->starts_with_special_prefix($valueOrObject)) {
			$this->shortName = $valueOrObject;
			$this->longName = $valueOrObject;
			$this->rank = self::RANK_WEEKDAY;
		} elseif ($this->starts_with_number($valueOrObject)) {
			$this->shortName = $valueOrObject;
			$this->longName = "the $valueOrObject";
			$this->rank = self::RANK_WEEKDAY;
		} elseif ($this->requires_definite_article($valueOrObject)) {
			$this->shortName = $valueOrObject;
			$this->longName = "the memorial of the $valueOrObject";
			$this->rank = self::RANK_MEMORIAL;
		} else {
			$this->shortName = $valueOrObject;
			$this->longName = "the memorial of $valueOrObject";
			$this->rank = self::RANK_MEMORIAL;
		}

		$this->colour = isset($extras->colours[$this->shortName])
			? $extras->colours[$this->shortName]
			: null;
	}

	/**
	 * @param string $str The string to check.
	 * @return bool Whether the string starts with the word 'dedication'.
	 */
	private function requires_definite_article($str) {
		if (is_null($str))
			return false;

		$matches = preg_match('/^dedication/i', $str);
		return $matches === 1;
	}

	/**
	 * @param string $str The string to check.
	 * @return bool Whether the string starts with a number.
	 */
	private function starts_with_number($str) {
		if (is_null($str))
			return false;

		$matches = preg_match('/^[0-9]/', $str);
		return $matches === 1;
	}

	/**
	 * @param string $str The string to check.
	 * @return bool Whether the string starts with a special prefix.
	 */
	private function starts_with_special_prefix($str) {
		if (is_null($str))
			return false;

		$firstWord = explode(' ', $str, 2)[0];
		return in_array($firstWord, self::SPECIAL_PREFIXES);
	}

	/**
	 * @param string $str The string to check.
	 * @return bool Whether the string starts with a weekday.
	 */
	private function starts_with_weekday($str) {
		if (is_null($str))
			return false;

		$firstWord = explode(' ', $str, 2)[0];
		return in_array($firstWord, self::DAYS_OF_WEEK);
	}
}
