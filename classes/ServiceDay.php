<?php

class ServiceDay implements JsonSerializable
{
	/** @var DateTime */
	public $date;

	/** @var Service[] */
	public $services;

	/**
	 * @param DateTime $date
	 * @param Service[] $services
	 */
	public function __construct($date, $services)
	{
		$this->date = $date;
		$this->services = $services;
	}

	/**
	 * Specifies data which should be serialized to JSON.
	 * @return array Data which can be serialized by json_encode.
	 */
	public function jsonSerialize()
	{
		return [
			'date' => $this->date->format('Y-m-d'),
			'services' => $this->services
		];
	}
}
