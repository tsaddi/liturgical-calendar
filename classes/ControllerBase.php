<?php

abstract class ControllerBase
{
	/** @var Log */
	private $log;

	/** @var string */
	private $requestBody;

	/** @var Settings */
	protected $settings;

	/**
	 * Gets a single item.
	 * @param string $date
	 */
	abstract protected function get($date);

	/**
	 * Gets a list of items.
	 */
	abstract protected function list();

	/**
	 * Adds an item.
	 * @param $date
	 */
	abstract protected function add($date);

	/**
	 * Deletes an item.
	 * @param $date
	 */
	abstract protected function delete($date);

	/**
	 * Updates an item.
	 * @param string $date
	 */
	abstract protected function update($date);

	/**
	 * Constructor.
	 * @param Settings $settings
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	public function __construct($settings)
	{
		$this->settings = $settings;
		$this->log = new Log($this->settings->logDirectory);

		$method = $_SERVER['REQUEST_METHOD'];
		$date = isset($_GET['date']) ? $_GET['date'] : '';
		switch ($method) {
			case 'GET':
				if (empty($date))
					$this->list();
				else
					$this->get($date);

				break;

			case 'POST':
				$this->add($date);
				break;

			case 'PUT':
				$this->update($date);
				break;

			case 'DELETE':
				$this->delete($date);
				break;

			default:
				$this->not_found();
				break;
		}
	}

	/**
	 * Sends a 400 Bad Request response.
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function bad_request() {
		$body = $this->response_body(['error' => 'Bad Request']);
		$this->send_response($body, 400);
	}

	/**
	 * Sends a 404 Not Found response.
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function not_found() {
		$body = $this->response_body(['error' => 'Not Found']);
		$this->send_response($body, 404);
	}

	/**
	 * Sends a 405 Method Not Allowed response.
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function method_not_allowed() {
		$body = $this->response_body(['error' => 'Method not allowed']);
		$this->send_response($body, 405);
	}

	/**
	 * @param array|null $body
	 * @return array
	 */
	private function response_body($body) {
		if (is_null($body))
			$body = [];

		if (ini_get('display_errors') === '1') {
			$backtrace = debug_backtrace();
			array_shift($backtrace); // This function
			$body['backtrace'] = $backtrace;
		}

		return $body;
	}

	/**
	 * @param mixed $response
	 * @param int $code
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function send_response($response, $code = 200) {
		http_response_code($code);
		header('Content-type: text/json');
		echo json_encode($response);

		$this->log->append($this->requestBody, $response);
	}

	/**
	 * @return mixed|null
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function get_body_json() {
		$this->requestBody = file_get_contents('php://input');
		if ($this->requestBody === false) {
			$this->bad_request();
			return null;
		}

		try {
			$this->requestBody = json_decode_throws($this->requestBody);
			return $this->requestBody;
		} catch (JsonEncodeDecodeException $e) {
			$this->bad_request();
			return null;
		}
	}

	/**
	 * @param string $date
	 * @return DateTime|null
	 */
	protected function parse_date($date) {
		if ($date === 'today') {
			return new DateTime();
		} elseif (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $date)) {
			$dateObj = DateTime::createFromFormat('!Y-m-d', $date);

			if ($dateObj === false)
				return null;

			$earliest = new DateTime();
			$earliest->setTime(0,0);

			$latest = new DateTime('+9 day');
			$latest->setTime(0,0);

			if ($dateObj < $earliest || $dateObj > $latest)
				return null;

			return $dateObj;
		} else {
			return null;
		}
	}
}
