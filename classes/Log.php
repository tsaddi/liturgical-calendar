<?php


class Log
{
	/** @var string */
	private $logDirectory;

	/** @var array  */
	private $log;

	/**
	 * @param string $logDirectory
	 */
	public function __construct($logDirectory)
	{
		$this->logDirectory = $logDirectory;
	}

	/**
	 * Writes a line to the log.
	 * @param mixed $request
	 * @param mixed $response
	 * @param mixed $details
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	public function append($request, $response, $details = null) {
		$now = new DateTime();
		$this->load($now);

		$this->log[] = (object)[
			'date' => $now->format('c'),
			'method' => $_SERVER['REQUEST_METHOD'],
			'uri' => $_SERVER['REQUEST_URI'],
			'user' => isset($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'] : null,
			'request' => $request,
			'response' => $response,
			'details' => $details
		];

		$filename = $this->filename($now);
		file_put_contents_throws($filename, json_encode_throws($this->log), LOCK_EX);
	}

	/**
	 * @param DateTime $date
	 * @return string
	 */
	private function filename($date) {
		$dateStr = $date->format('Y-m-d');
		$filename = join_paths(APP_ROOT, $this->logDirectory, "$dateStr.json");
		return $filename;
	}

	/**
	 * @param DateTime $date
	 * @return array
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	private function load($date) {
		if (isset($this->log))
			return $this->log;

		$filename = $this->filename($date);
		if (file_exists($filename)) {
			$this->log = json_decode_throws(file_get_contents_non_blocking($filename));
		} else {
			// No log file yet today
			$this->log = [];
		}

		return $this->log;
	}
}
