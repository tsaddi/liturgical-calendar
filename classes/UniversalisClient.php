<?php

class UniversalisClient {
	/** @var string */
	private $cacheFile;
	/** @var string */
	private $calendarUrl;
	/** @var string */
	private $callback;
	/** @var Day[] */
	private $days = [];
	/** @var DayExtras[] */
	private $additionalInfo = [];
	/** @var string */
	private $url;

	/**
	 * UniversalisClient constructor.
	 * @param string $calendarUrl
	 * @param string $url
	 * @param string $callback
	 * @param string $cacheFie
	 */
	public function __construct($calendarUrl, $url, $callback, $cacheFie)
	{
		$this->calendarUrl = $calendarUrl;
		$this->url = $url;
		$this->callback = $callback;
		$this->cacheFile = $cacheFie;

		$this->loadCache();
	}

	/**
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	public function __destruct()
	{
		$this->saveCache();
	}

	/**
	 * @param DateTime $date
	 * @return Day|null
	 */
	public function get_day($date) {
		$dateStr = $date->format('Ymd');
		if (array_key_exists($dateStr, $this->days))
			return $this->days[$dateStr];

		$url = str_replace('{{DATE}}', $dateStr, $this->url);

		try {
			$jsonp = curl_get($url);
		} catch (CurlException $e) {
			return null;
		}

		if (is_null($jsonp) || preg_match('/^\s$/', $jsonp))
			return null;

		$json = substr($jsonp, strlen($this->callback) + 1, strlen($jsonp) - strlen($this->callback) - 4);

		try {
			$day = new Day($date, json_decode_throws($json)->day, $this->get_additional_info($date));
		} catch (JsonEncodeDecodeException $e) {
			return null;
		}

		if (!is_null($day))
			$this->days[$dateStr] = $day;

		return $day;
	}

	/**
	 * @param DateTime $date
	 * @return DayExtras | null
	 */
	private function get_additional_info($date) {
		$dateStr = $date->format('Ymd');
		if (!array_key_exists($dateStr, $this->additionalInfo))
			$this->populate_additional_info($date->format('Y'));

		if (!array_key_exists($dateStr, $this->additionalInfo))
			return null;

		return $this->additionalInfo[$dateStr];
	}

	private function populate_additional_info($year) {
		$url = str_replace('{{YEAR}}', $year, $this->calendarUrl);

		try {
			$html = curl_get($url);
		} catch (CurlException $e) {
			return;
		}

		if (is_null($html) || preg_match('/^\s$/', $html))
			return;

		// Look for the start of the table.
		$offset = strpos($html, '<table id="yearly-calendar"');
		if ($offset === false)
			return;

		$lines = preg_split('/\r\n|\n|\r/', substr($html, $offset));

		$colourSubPattern = 'class="lit-(?<colour>' . DayBase::COLOUR_BLACK . '|' . DayBase::COLOUR_GREEN . '|' . DayBase::COLOUR_VIOLET . '|' . DayBase::COLOUR_RED . '|' . DayBase::COLOUR_ROSE . '|' . DayBase::COLOUR_WHITE . ')"';
		$monthHeaderPattern = /** @lang PhpRegExp */ '/<th colspan="2">(?<month>January|February|March|April|May|June|July|August|September|October|November|December)<\/th>/';
		$dayPattern = /** @lang PhpRegExp */ '/>(?:Sun|Mon|Tue|Wed|Thu|Fri|Sat)&#160;(?<dayNumber>[1-3]?[0-9])<\/a><\/td><td>(?:<span class="rank-[0-9]{1,2}">)?(?<dayName>[^<]+)?/';
		$dayWithColourPattern = /** @lang PhpRegExp */ '/>(?:Sun|Mon|Tue|Wed|Thu|Fri|Sat)&#160;(?<dayNumber>[1-3]?[0-9])<\/a><\/td><td>(?:<span class="rank-[0-9]{1,2}">)?(?<dayName>[^<]+).*' . $colourSubPattern . '/';
		$memorialPattern = /** @lang PhpRegExp */ '/^<br>or (?<memorialName>[^<&]+)$/';
		$colourPattern = /** @lang PhpRegExp */ "/$colourSubPattern/";
		$psalterWeekNumberPattern = /** @lang PhpRegExp */ '/>Psalm week (?<psalterWeekNumber>[1-4])/';

		$today = new DateTime();
		$today->setTime(0,0);

		$month = null;
		$date = null;
		$dayName = null;
		$memorialName = null;
		$colour = null;
		$psalterWeekNumber = null;
		foreach ($lines as $line) {
			if (isset($dayName) && preg_match($psalterWeekNumberPattern, $line, $matches)) {
				$psalterWeekNumber = $matches['psalterWeekNumber'];
				if ($date >= $today)
					$this->set_additional_info($date, null, null, $psalterWeekNumber);
			} elseif (isset($dayName) && preg_match($memorialPattern, $line, $matches)) {
				$memorialName = html_entity_decode($matches['memorialName']);
				$colour = null;
			} elseif (isset($month) && (preg_match($dayWithColourPattern, $line, $matches) || preg_match($dayPattern, $line, $matches))) {
				$date = new DateTime("${matches['dayNumber']} $month $year");
				$dayName = html_entity_decode($matches['dayName']);
				$memorialName = null;
				if (empty($matches['colour'])) {
					$colour = null;
				} else {
					$colour = $matches['colour'];
					if ($date >= $today)
						$this->set_additional_info($date, $dayName, $colour, $psalterWeekNumber);
				}
			} elseif (isset($dayName) && !isset($colour) && preg_match($colourPattern, $line, $matches)) {
				$name = is_null($memorialName) ? $dayName : $memorialName;
				$memorialName = null;
				$colour = null;
				if ($date >= $today)
					$this->set_additional_info($date, $name, $matches['colour'], $psalterWeekNumber);
			} elseif (preg_match($monthHeaderPattern, $line, $matches)) {
				$month = $matches['month'];
				$date = null;
				$dayName = null;
				$memorialName = null;
				$colour = null;
			} elseif (strpos($line, '</table>') !== FALSE) {
				// We're at the end of the table.
				break;
			}
		}

		foreach ($this->days as $dateStr => &$day) {
			if (isset($this->additionalInfo[$dateStr])) {
				$additionalInfo = $this->additionalInfo[$dateStr];

				$day->colour = $additionalInfo->colours[$day->shortName];
				$day->psalterWeekNumber = $additionalInfo->psalterWeekNumber;

				foreach ($day->optionalMemorials as &$memorial) {
					$memorial->colour = $additionalInfo->colours[$memorial->shortName];
					$memorial->psalterWeekNumber = $additionalInfo->psalterWeekNumber;
				}
			}
		}
	}

	/**
	 * @param DateTime $date
	 * @param string | null $dayName
	 * @param string | null $colour
	 * @param int $psalterWeekNumber
	 */
	private function set_additional_info($date, $dayName, $colour, $psalterWeekNumber) {
		$dateStr = $date->format('Ymd');

		if (!isset($this->additionalInfo[$dateStr]))
			$this->additionalInfo[$dateStr] = new DayExtras();

		if (!is_null($dayName) && !is_null($colour))
			$this->additionalInfo[$dateStr]->colours[$dayName] = $colour;

		if (!is_null($psalterWeekNumber))
			$this->additionalInfo[$dateStr]->psalterWeekNumber = $psalterWeekNumber;
	}

	private function loadCache() {
		if (!file_exists($this->cacheFile)) {
			// No cache file yet
			$this->days = [];
			return;
		}

		try {
			$data = json_decode_throws(file_get_contents_non_blocking($this->cacheFile));
		} catch (FileException $e) {
			// Throw away the cache
			$data = [];
		} catch (JsonEncodeDecodeException $e) {
			// Throw away the cache
			$data = [];
		}

		$this->days = [];
		foreach ($data->days as $sateStr => $day)
			$this->days[$sateStr] = new Day($day);

		$this->additionalInfo = (array)$data->additionalInfo;

		$this->cleanCache();
	}

	private function cleanCache() {
		$today = new DateTime();
		$today->setTime(0,0);

		foreach ($this->days as $dateStr => $day)
			$this->clean_cache_entry($this->days, $dateStr, $today);

		foreach ($this->additionalInfo as $dateStr => $additionalInfo)
			$this->clean_cache_entry($this->additionalInfo, $dateStr, $today);
	}

	/**
	 * @param array $arr
	 * @param string $dateStr
	 * @param DateTime $today
	 */
	private function clean_cache_entry(&$arr, $dateStr, $today) {
		try {
			$date = new DateTime($dateStr);
		} catch (Exception $e) {
			// Invalid date, delete this cache entry
			unset($arr[$dateStr]);
			return;
		}

		if ($date < $today) {
			// Date in te past, delete this cache entry
			unset($arr[$dateStr]);
		}
	}

	/**
	 * @throws FileException Could not write file.
	 * @throws JsonEncodeDecodeException Error encoding JSON.
	 */
	private function saveCache() {
		$cache = [
			'days' => $this->days,
			'additionalInfo' => $this->additionalInfo
		];

		file_put_contents_throws($this->cacheFile, json_encode_throws($cache), LOCK_EX);
	}
}
