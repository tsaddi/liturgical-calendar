<?php

class Services {
	/** @var string Filesystem path of the cache file. */
	private $cacheFile;
	/** @var Service[][] List of regular services for each day (Sunday is the first day of the week). */
	private $defaultServices;
	/** @var Service[][] */
	private $days = [];
	/** @var bool Whether this instance of this class is read-only. */
	private $readOnly;

	/**
	 * Selection constructor.
	 * @param string $cacheFie Filesystem path of the cache file.
	 * @param Service[][] $defaultServices List of regular services for each day (Sunday is the first day of the week).
	 * @param bool $readOnly Whether to construct a read-only instance of this class.
	 */
	public function __construct($cacheFie, $defaultServices, $readOnly = false)
	{
		$this->cacheFile = $cacheFie;
		$this->defaultServices = $defaultServices;
		$this->readOnly = $readOnly;
		$this->loadCache();
	}

	/**
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	public function __destruct()
	{
		if (!$this->readOnly)
			$this->saveCache();
	}

	/**
	 * @param DateTime $date
	 * @return Service[]
	 */
	public function get_day($date) {
		$dateStr = $date->format('Ymd');
		if (array_key_exists($dateStr, $this->days))
			return $this->days[$dateStr];

		return $this->defaultServices[$date->format('w')];
	}

	/**
	 * @param DateTime $date
	 * @param string $hash
	 * @param Service $service
	 * @throws Exception Hash not found on that day.
	 * @throws Exception Services class was constructed as read-only.
	 */
	public function set_day($date, $hash, $service) {
		if ($this->readOnly)
			throw new Exception("Services class was constructed as read-only.");

		$dateStr = $date->format('Ymd');

		if (!array_key_exists($dateStr, $this->days))
			$this->days[$dateStr] = $this->defaultServices[$date->format('w')];

		foreach ($this->days[$dateStr] as &$s) {
			if ($s->hash === $hash) {
				$s = $service;
				return;
			}
		}

		throw new Exception('Hash not found on that day.');
	}

	/**
	 * @param DateTime $date
	 * @param string $hash
	 * @throws Exception Hash not found on that day.
	 * @throws Exception Services class was constructed as read-only.
	 */
	public function delete($date, $hash) {
		if ($this->readOnly)
			throw new Exception("Services class was constructed as read-only.");

		$dateStr = $date->format('Ymd');

		if (!array_key_exists($dateStr, $this->days))
			$this->days[$dateStr] = $this->defaultServices[$date->format('w')];

		foreach ($this->days[$dateStr] as $key => $s) {
			if ($s->hash === $hash) {
				unset($this->days[$dateStr][$key]);
				return;
			}
		}

		throw new Exception('Hash not found on that day.');
	}

	/**
	 * @param DateTime $date
	 * @param Service $service
	 * @throws Exception Services class was constructed as read-only.
	 */
	public function add($date, $service) {
		if ($this->readOnly)
			throw new Exception("Services class was constructed as read-only.");

		$dateStr = $date->format('Ymd');

		if (!array_key_exists($dateStr, $this->days))
			$this->days[$dateStr] = $this->defaultServices[$date->format('w')];

		$this->days[$dateStr][] = $service;
	}

	private function loadCache() {
		if (!file_exists($this->cacheFile)) {
			// No cache file yet
			$this->days = [];
			return;
		}

		try {
			$data = json_decode_throws(file_get_contents_non_blocking($this->cacheFile));
		} catch (FileException $e) {
			// Throw away the cache
			$data = [];
		} catch (JsonEncodeDecodeException $e) {
			// Throw away the cache
			$data = [];
		}

		$this->days = [];
		foreach ($data->days as $dateStr => $day) {
			$services = [];
			foreach ($day as $service) {
				$services[] = new Service($service);
			}
			$this->days[$dateStr] = $services;
		}

		$this->cleanCache();
	}

	private function cleanCache() {
		if ($this->readOnly)
			return;

		$today = new DateTime();
		$today->setTime(0,0);

		foreach ($this->days as $dateStr => $day)
			$this->clean_cache_entry($this->days, $dateStr, $today);
	}

	/**
	 * @param array $arr
	 * @param string $dateStr
	 * @param DateTime $today
	 */
	private function clean_cache_entry(&$arr, $dateStr, $today) {
		try {
			$date = new DateTime($dateStr);
		} catch (Exception $e) {
			// Invalid date, delete this cache entry
			unset($arr[$dateStr]);
			return;
		}

		if ($date < $today) {
			// Date in te past, delete this cache entry
			unset($arr[$dateStr]);
		}
	}

	/**
	 * @throws FileException Could not write file.
	 * @throws JsonEncodeDecodeException Error encoding JSON.
	 * @throws Exception Services class was constructed as read-only.
	 */
	private function saveCache() {
		if ($this->readOnly)
			throw new Exception("Services class was constructed as read-only.");

		$cache = [
			'days' => $this->days
		];

		file_put_contents_throws($this->cacheFile, json_encode_throws($cache), LOCK_EX);
	}
}
