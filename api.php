<?php
/** @noinspection PhpIncludeInspection */
/** @noinspection PhpUnhandledExceptionInspection */

define('APP_ROOT', __DIR__);

spl_autoload_register(function ($class) {
	require(APP_ROOT . '/classes/' . $class . '.php');
});

foreach (glob(APP_ROOT . '/includes/*.php') as $filename)
	require($filename);

$controller = ucfirst($_GET['action']) . 'Controller';
$controllerPath = APP_ROOT . '/controllers/' . $controller . '.php';
if (file_exists($controllerPath)) {
	require($controllerPath);
	new $controller();
} else {
	http_response_code(404);
	header('Content-type: text/json');
	echo json_encode(['error' => 'Not Found']);
}
