<?php

class ServicesController extends ControllerBase
{
	/** @var Services */
	private $services;

	public function __construct()
	{
		$settings = new Settings();

		$this->services = new Services(
			join_paths(APP_ROOT, $settings->servicesCacheFile),
			$settings->defaultServices
		);

		parent::__construct($settings);
	}

	/**
	 * Gets the info for the given day.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function get($date)
	{
		$dateObj = $this->parse_date($date);
		if (is_null($dateObj)) {
			$this->not_found();
			return;
		}

		$this->send_response(new ServiceDay($dateObj, $this->services->get_day($dateObj)));
	}

	/**
	 * Gets the info for the next ten days.
	 */
	protected function list()
	{
		$selection = [];
		for ($i = 0; $i < 10; $i++) {
			$date = new DateTime("+$i day");
			$selection[] = new ServiceDay($date, $this->services->get_day($date));
		}

		$this->send_response([
			'services' => $selection
		]);
	}

	/**
	 * Adds a service on the given day.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function add($date)
	{
		$dateObj = $this->parse_date($date);
		if (is_null($dateObj)) {
			$this->not_found();
			return;
		}

		$service = $this->get_body_json();
		if (!$service) {
			$this->bad_request();
			return;
		}

		$service = new Service($service);
		$this->services->add($dateObj, $service);
		$this->send_response($service);
	}

	/**
	 * Updates the selection for the given day.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function update($date) {
		$dateObj = $this->parse_date($date);
		if (is_null($dateObj)) {
			$this->not_found();
			return;
		}

		$hash = $this->get_hash($dateObj);
		if ($hash === false) {
			$this->not_found();
			return;
		}

		$service = $this->get_body_json();
		if (!$service) {
			$this->bad_request();
			return;
		}

		$service = new Service($service);
		$this->services->set_day($dateObj, $hash, $service);
		$this->send_response($service);
	}

	/**
	 * Deletes the service with the given hash.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function delete($date)
	{
		$dateObj = $this->parse_date($date);
		if (is_null($dateObj)) {
			$this->not_found();
			return;
		}

		$hash = $this->get_hash($dateObj);
		if ($hash === false) {
			$this->not_found();
			return;
		}

		$this->services->delete($dateObj, $hash);
		$this->send_response([]);
	}

	/**
	 * @param DateTime $dateObj
	 * @return string|false
	 */
	private function get_hash($dateObj)
	{
		$hash = isset($_GET['hash']) ? $_GET['hash'] : '';

		$noSuchService = true;
		$services = $this->services->get_day($dateObj);
		foreach ($services as $s) {
			if ($s->hash === $hash) {
				$noSuchService = false;
				break;
			}
		}

		if ($noSuchService)
			return false;

		return $hash;
	}
}
