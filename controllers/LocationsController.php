<?php

class LocationsController extends ControllerBase
{
	public function __construct()
	{
		parent::__construct(new Settings());
	}

	/**
	 * Not implemented.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function get($date) { $this->method_not_allowed(); }

	/**
	 * Gets all locations.
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function list()
	{
		$this->send_response([
			'locations' => $this->settings->locations
		]);
	}

	/**
	 * Not implemented.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function add($date) { $this->method_not_allowed(); }

	/**
	 * Not implemented.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function update($date) { $this->method_not_allowed(); }

	/**
	 * Not implemented.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function delete($date) { $this->method_not_allowed(); }
}
