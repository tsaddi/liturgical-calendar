<?php

class DaysController extends ControllerBase
{
	/** @var Selection */
	private $selection;

	/** @var UniversalisClient */
	private $universalis;

	public function __construct()
	{
		$settings = new Settings();

		$this->selection = new Selection(join_paths(APP_ROOT, $settings->selectionCacheFile));
		$this->universalis = new UniversalisClient(
			$settings->universalisCalendarUrl,
			$settings->universalisUrl,
			$settings->universalisCallback,
			join_paths(APP_ROOT, $settings->universalisCacheFile)
		);

		parent::__construct($settings);
	}

	/**
	 * Gets the info for the given day.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function get($date)
	{
		$dateObj = $this->parse_date($date);
		if (is_null($dateObj)) {
			$this->not_found();
			return;
		}

		$day = $this->universalis->get_day($dateObj);
		$selection = $this->selection_or_default(
			$day,
			$this->selection->get_day($dateObj)
		);

		$this->send_response([
			'day' => $day,
			'selection' => $selection
		]);
	}

	/**
	 * Gets the info for the next ten days.
	 */
	protected function list()
	{
		$days = [];
		$selection = [];
		for ($i = 0; $i < 10; $i++) {
			$date = new DateTime("+$i day");
			$day = $this->universalis->get_day($date);
			if (!is_null($day)) {
				$days[] = $day;
				$selection[] = $this->selection_or_default(
					$day,
					$this->selection->get_day($date)
				);
			}
		}

		$this->send_response([
			'days' => $days,
			'selection' => $selection
		]);
	}

	/**
	 * Not implemented.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function add($date) { $this->method_not_allowed(); }

	/**
	 * Updates the selection for the given day.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function update($date)
	{
		$dateObj = $this->parse_date($date);
		if (is_null($dateObj)) {
			$this->not_found();
			return;
		}

		$selection = $this->get_body_json();
		$day = $this->universalis->get_day($dateObj);
		$selection = new DaySelection((object)[
			'shortName' => $selection->shortName,
			'longName' => $selection->longName,
			'rank' => $selection->rank,
			'colour' => $selection->colour,
			'date' => $dateObj->format('Y-m-d'),
			'psalterWeekNumber' => $day->psalterWeekNumber
		]);

		$this->selection->set_day($dateObj, $selection);

		$this->send_response($selection);
	}

	/**
	 * Not implemented.
	 * @param string $date
	 * @throws FileException
	 * @throws JsonEncodeDecodeException
	 */
	protected function delete($date) { $this->method_not_allowed(); }

	/**
	 * @param Day $day
	 * @param DaySelection $selection
	 * @return DaySelection
	 */
	private function selection_or_default($day, $selection) {
		if (!is_null($selection))
			return $selection;

		return new DaySelection((object)[
			'shortName' => $day->shortName,
			'longName' => $day->longName,
			'rank' => $day->rank,
			'colour' => $day->colour,
			'date' => $day->date->format('Y-m-d'),
			'psalterWeekNumber' => $day->psalterWeekNumber
		]);
	}
}
