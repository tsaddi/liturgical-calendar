<?php

/**
 * Send a GET request using cURL
 * @param string $url The URL to fetch.
 * @return string The the response body.
 * @throws CurlException An error occurred.
 */
function curl_get($url)
{
	$ch = curl_init();
	curl_setopt_array($ch, [
		CURLOPT_URL => $url,
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => TRUE
	]);

	$result = curl_exec($ch);
	if($result === FALSE)
		throw new CurlException(curl_error($ch));

	curl_close($ch);
	return $result;
}

class CurlException extends Exception { }
