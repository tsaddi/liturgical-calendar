<?php

/**
 * @param string $str The json string being decoded.
 * @param bool $assoc When TRUE, returned objects will be converted into associative arrays.
 * @param int $depth User specified recursion depth.
 * @param int $options Bitmask of JSON decode options (see {@see json_decode}).
 * @return mixed The value encoded in json in appropriate PHP type. Values true, false and null (case-insensitive) are returned as TRUE, FALSE and NULL respectively.
 * @throws JsonEncodeDecodeException The JSON cannot be decoded or the encoded data is deeper than the recursion limit.
 */
function json_decode_throws($str, $assoc = false, $depth = 512, $options = 0) {
	if ($str == '')
		throw new JsonEncodeDecodeException('Invalid JSON: Empty string');

	if (strtolower($str) == 'null')
		return null;

	$json = json_decode($str, $assoc, $depth, $options);
	if (is_null($json))
		throw new JsonEncodeDecodeException(
			'Invalid JSON: ' .
			json_error_description(json_last_error())
		);

	return $json;
}

/**
 * @param mixed $value The value being encoded. Can be any type except a resource.
 * @param int $options See json_encode for available options.
 * @param int $depth Set the maximum depth. Must be greater than zero.
 * @return string The value encoded as a JSON string.
 * @throws JsonEncodeDecodeException Error encoding JSON.
 */
function json_encode_throws($value, $options = 0, $depth = 512) {
	$str = json_encode($value, $options, $depth);
	if ($str === FALSE)
		throw new JsonEncodeDecodeException(
			'Error encoding JSON: ' .
			json_error_description(json_last_error())
		);

	return $str;
}

function json_error_description($errorId) {
	switch ($errorId) {
		case JSON_ERROR_NONE:
			return 'No error has occurred';
		case JSON_ERROR_DEPTH:
			return 'The maximum stack depth has been exceeded';
		case JSON_ERROR_STATE_MISMATCH:
			return 'Invalid or malformed JSON';
		case JSON_ERROR_CTRL_CHAR:
			return 'Control character error, possibly incorrectly encoded';
		case JSON_ERROR_SYNTAX:
			return 'Syntax error';
		case JSON_ERROR_UTF8:
			return 'Malformed UTF-8 characters, possibly incorrectly encoded';
		case JSON_ERROR_RECURSION:
			return 'One or more recursive references in the value to be encoded';
		case JSON_ERROR_INF_OR_NAN:
			return 'One or more NAN or INF values in the value to be encoded';
		case JSON_ERROR_UNSUPPORTED_TYPE:
			return 'A value of a type that cannot be encoded was given';
		case JSON_ERROR_INVALID_PROPERTY_NAME:
			return 'A property name that cannot be encoded was given';
		case JSON_ERROR_UTF16:
			return 'Malformed UTF-16 characters, possibly incorrectly encoded';
		default:
			return 'Unknown error';
	}
}

class JsonEncodeDecodeException extends Exception { }
