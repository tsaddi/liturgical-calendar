<?php

/**
 * @param string $filename Name of the file to read.
 * @param bool $use_include_path Triggers include path search.
 * @param resource|null $context A valid context resource created with stream_context_create.
 * @return string The read data.
 * @throws FileException File not found
 * @throws FileException Could not open file
 * @throws FileException File is locked.
 */
function file_get_contents_non_blocking($filename, $use_include_path = false, $context = null) {
	if (!file_exists($filename))
		throw new FileException("File not found: $filename");

	$handle = is_null($context)
		? fopen($filename, 'r', $use_include_path)
		: fopen($filename, 'r', $use_include_path, $context);

	if ($handle === FALSE)
		throw new FileException("Could not open file: $filename");

	if (!flock($handle, LOCK_SH | LOCK_NB)) {
		fclose($handle);
		throw new FileException("File is locked: $filename");
	}

	$contents = '';
	while (!feof($handle))
		$contents .= fread($handle, 8192);

	fclose($handle);

	return $contents;
}

/**
 * @param string $filename Name of the file to read.
 * @param mixed $data The data to write. Can be either a string, an array or a stream resource.
 * @param int $flags See file_put_contents for available flags.
 * @param resource|null $context A valid context resource created with stream_context_create.
 * @throws FileException Could not write file
 */
function file_put_contents_throws($filename, $data, $flags = 0, $context = null) {
	$result = file_put_contents($filename, $data, $flags, $context);
	if ($result === FALSE)
		throw new FileException("Could not write file: $filename");
}

/**
 * @see https://stackoverflow.com/a/15575293
 * @return string
 */
function join_paths() {
	$paths = array();

	foreach (func_get_args() as $arg) {
		if ($arg !== '') { $paths[] = $arg; }
	}

	return preg_replace('#/+#','/',join('/', $paths));
}

class FileException extends Exception { }
