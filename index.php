<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Liturgical Calendar</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <meta name="theme-color" content="#ffa100">
    <meta name="color-scheme" content="light dark" />

    <link href="https://unpkg.com/@ionic/core@latest/css/ionic.bundle.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">

    <script type="module" src="https://unpkg.com/@ionic/core@latest/dist/ionic/ionic.esm.js"></script>
    <script nomodule="" src="https://unpkg.com/@ionic/core@latest/dist/ionic/ionic.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@latest/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule="" src="https://unpkg.com/ionicons@latest/dist/ionicons/ionicons.js"></script>
</head>
<body>

<app-main></app-main>

<?php

foreach (glob(__DIR__ . '/app/components/*.js') as $filename)
    echo '<script src="app/components/' . basename($filename) . '"></script>';

foreach (glob(__DIR__ . '/app/utilities/*.js') as $filename)
	echo '<script src="app/utilities/' . basename($filename) . '"></script>';

?>
</body>
</html>
